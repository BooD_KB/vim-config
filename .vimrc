map <F5> :NERDTreeToggle<CR>
map <F6> :TagbarToggle<CR>
map gn :tabnew<CR>
set number
set tabstop=4
set shiftwidth=4
map <F2> :w!<CR>
filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

filetype plugin indent on

filetype plugin on


set fileencodings=utf8,cp1251

"let g:clang_library_path = "/usr/lib"

syn on
set tabstop=4 
set shiftwidth=4
set smarttab
set expandtab "Ставим табы пробелами
set softtabstop=4 "4 пробела в табе
set autoindent
set foldcolumn=1

set laststatus=2
set noshowmode

set t_Co=256

let g:tagbar_autofocus=1

" python settings
let python_highlight_all = 1


" C settings
"let g:clang_snippets=1
let g:clang_hl_errors=1
"let g:clang_conceal_snippets=1
let g:clang_complete_auto=1
let g:clang_complete_copen= 1
let g:clang_auto_select= 0
let g:clang_periodic_quickfix= 1
let g:clang_snippets=1
let g:clang_trailing_placeholder=1
let g:clang_close_preview=1
let g:clang_complete_patterns=1
let g:clang_snippets=1

""Plugins (vundle settings)""
Bundle 'gmarik/vundle'

"Bundle 'scrooloose/nerdtree'
"Bundle 'msanders/snipmate.vim'
"Bundle 'dfrank_util'
"Bundle 'vim-scripts/vimprj'
"Bundle 'majutsushi/tagbar'
"Bundle 'vim-scripts/indexer.tar.gz'
"Bundle 'jlanzarotta/bufexplorer'
"Bundle 'Python-mode-klen'
"Bundle 'tpope/vim-fugitive'
"Bundle 'bling/vim-airline'
"Bundle 'zeis/vim-kolor'
"Bundle 'indexer.tar.gz'
"Bundle 'clang-complete'
"Bundle 'c.vim'
"Bundle 'a.vim'
"Bundle 'c-standard-functions-highlight'
"Bundle 'C-fold'
"Bundle 'xml.vim'
"""""""""""""""""""""""""""""

colorscheme kolor

let pymode_lint_ignore="C0110,C1001,C0301,E501,R0914,C0111,R0903,R0201,C0103,E303"
let pymode_lint_jump="1"

" Airline settings
  if !exists('g:airline_symbols')
    let g:airline_symbols = {}
  endif
"
"  " unicode symbols
  let g:airline_left_sep = '»'
  let g:airline_left_sep = '▶'
  let g:airline_right_sep = '«'
  let g:airline_right_sep = '◀'
  let g:airline_symbols.linenr = '␊'
  let g:airline_symbols.linenr = '␤'
  let g:airline_symbols.linenr = '¶'
  let g:airline_symbols.branch = '⎇ '
  let g:airline_symbols.paste = 'ρ'
  let g:airline_symbols.paste = 'Þ'
  let g:airline_symbols.paste = '∥'
  let g:airline_symbols.whitespace = 'Ξ'
"
"
  let g:airline_theme='badwolf'
  let g:airline_enable_bufferline=1
  let g:airline#extensions#branch#enabled = 1
  let g:airline#extensions#tagbar#enabled = 1
"
"  "let g:airline_section_c = '%t'
"
  "let g:airline#extensions#tabline#left_sep = '>'
  "let g:airline#extensions#tabline#left_alt_sep = '|'

  "let g:airline#extensions#tabline#enabled = 1
